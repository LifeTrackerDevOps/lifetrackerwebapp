import React from "react";

import RegisterFormScreen from '../presentational/RegisterFormScreen';

import LifeTrackerHttpRequest from '../utils/LifeTrackerHttpRequest';

export default class RegisterForm extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            emailValue: '',
            passwordValue: '',
            passwordConfirmValue: '',
            canSubmit: false,
        };

        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.handleConfirmPasswordChange = this.handleConfirmPasswordChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleEmailChange(event){
        if(String(event.target.value).match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)){
            this.setState({canSubmit: true});
        }else{
            this.setState({canSubmit: false});
        }
        this.setState({emailValue: event.target.value});
    }
    handlePasswordChange(event){
        if(event.target.value != this.state.passwordConfirmValue){
            this.setState({canSubmit: false});
        }else{
            if(!String(event.target.value).match(/[^a-z0-9!@#$%^&*]/gi)){
                this.setState({canSubmit: true});
            }else{
                this.setState({canSubmit: false});
            }
        }
        this.setState({passwordValue: event.target.value});
    }
    handleConfirmPasswordChange(event){
        if(this.state.passwordValue != event.target.value){
            this.setState({canSubmit: false});
        }else{
            if(!String(event.target.value).match(/[^a-z0-9!@#$%^&*]/gi)){
                this.setState({canSubmit: true});
            }else{
                this.setState({canSubmit: false});
            }
        }
        this.setState({passwordConfirmValue: event.target.value});
    }
    handleSubmit(event){
        event.preventDefault();
        new LifeTrackerHttpRequest().post("api/register", { email: this.state.emailValue, password: this.state.passwordValue}).then(value => {
            if(value == true){
                alert("Goodf");
            }else{
                alert("Bad");
            }
        });
    }
    render(){
        return (
            <RegisterFormScreen
                onSubmitFunction={this.handleSubmit}
                canSubmit={this.state.canSubmit}
                onChangeEmailFunction={this.handleEmailChange}
                onChangePasswordFunction={this.handlePasswordChange}
                onChangeConfirmPasswordFunction={this.handleConfirmPasswordChange}
                emailValue={this.state.emailValue}
                passwordValue={this.state.passwordValue}
                confirmPasswordValue={this.state.passwordConfirmValue}
            />
        );
    }
}