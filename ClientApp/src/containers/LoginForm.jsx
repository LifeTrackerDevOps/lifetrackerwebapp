import React from "react";

import LoginFormScreen from '../presentational/LoginFormScreen';

import LifeTrackerHttpRequest from '../utils/LifeTrackerHttpRequest';

import { connect } from "react-redux";

import { setEmailField, setPasswordField, attemptLogin } from "../actions/authActions"

@connect((store) => {
    return {
        handle: store.auth.attemptedValues
    }
})
export default class LoginForm extends React.Component {
    constructor(props){
        super(props);

        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleEmailChange(event){
        this.props.dispatch(setEmailField(event));
    }
    handlePasswordChange(event){
        this.props.dispatch(setPasswordField(event));
    }
    handleSubmit(event){
        event.preventDefault();
        this.props.dispatch(attemptLogin(this.props.handle.email, this.props.handle.password));
    }
    render(){
        return (
        <div>
            <LoginFormScreen
                onSubmitFunction={this.handleSubmit}
                onChangeUsernameFunction={this.handleEmailChange}
                onChangePasswordFunction={this.handlePasswordChange}
            />
        </div>
        );
    }
}