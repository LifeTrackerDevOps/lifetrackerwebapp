import React from "react";

import LogoutButtonScreen from '../presentational/LogoutButtonScreen';

import LifeTrackerHttpRequest from '../utils/LifeTrackerHttpRequest';

export default class LogoutButton extends React.Component {
    constructor(props){
        super(props);
        this.logout = this.logout.bind(this);
    }
    logout(){
        new LifeTrackerHttpRequest().post("api/logout").then(value => {
            if(value == true){
                alert("Logged out!");
            }else{
                alert("Not logged in!");
            }
        });
    }
    render(){
        return (
            <LogoutButtonScreen handleLogout={this.logout}/>       
        );
    }
}