import React from "react";

import LoginForm from "./containers/LoginForm";
import RegisterForm from "./containers/RegisterForm";
import LogoutButton from "./containers/LogoutButton";

import HomeScene from "./scenes/HomeScene";
import OverviewScene from "./scenes/OverviewScene";

import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";

import { connect } from "react-redux";

import { setLoginStatus } from "./actions/authActions";

import Cookies from "js-cookie";

@connect((store) => {
    return {
        auth: store.auth
    }
})
export default class App extends React.Component {
    componentWillMount() {
        const logginValue = Cookies.get("ln_flag");
        if(logginValue != null && logginValue != "" && this.props.auth.loggedIn == false) {
            this.props.dispatch(setLoginStatus(true));
        }
    }
    render() {
        return (
            <BrowserRouter>
                <Switch>
                    <Route exact path="/" component={HomeScene}/>
                    <Route exact path="/overview" component={() => (
                        !this.props.auth.loggedIn ? (<Redirect to="/"/>) : 
                        (<OverviewScene/>)
                    )}/>
                    <Route exact path="/register" component={RegisterForm}/>
                </Switch>
            </BrowserRouter>
        );
    }
}