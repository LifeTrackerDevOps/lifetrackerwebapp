import Cookies from 'js-cookie';

export default class LifeTrackerHttpRequest{
    post(url, rawData = {}) {
        let formData = new FormData();
        Object.keys(rawData).forEach(element => {
            formData.append(element, rawData[element]);
        });
        return fetch(url, {
            method: "post",
            credentials: 'include',
            headers: {
                "X-CSRFToken": Cookies.get("csrftoken")
            },
            body: formData
        })
        .then(function status(response) {
            if (response.status >= 200 && response.status < 300) {
                return Promise.resolve(response)
            } else {
                return Promise.reject(new Error(response.statusText))
            }
        })
        .then(function(data){
            console.log('Request succeeded with response', data.body);
            return true;
        })
        .catch(function(error) {
            console.error('Request failed', error);
            return false;
        });
    }
}