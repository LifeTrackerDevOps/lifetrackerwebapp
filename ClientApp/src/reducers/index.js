import { combineReducers,Reducer } from "redux";

import authReducer from "./authReducer";

export default combineReducers({
    auth: authReducer
});