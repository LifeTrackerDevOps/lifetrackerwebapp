const initialState = {
    loggedIn: false,
    justRejected: false,
    email: "",
    attemptedValues: {
        email: undefined,
        password: undefined
    }
}
export default function authReducer(state=initialState, action) {
    switch(action.type){
        case "USER_AUTH_FULFILLED": {
            state = {
                ...state,
                loggedIn: true,
                justRejected: false,
                email: state.attemptedValues.email,
                attemptedValues: {
                    ...state.attemptedValues,
                    password: undefined,
                    email: undefined
                }
            };
            break;
        }
        case "USER_AUTH_REJECTED": {
            state = {
                ...state,
                loggedIn: false,
                justRejected: true
            };
            break;
        }
        case "USER_AUTH_SET_ATTEMPTED_EMAIL": {
            state = {
                ...state,
                attemptedValues: {
                    ...state.attemptedValues,
                    email: action.payload
                }
            };
            break;
        }
        case "USER_AUTH_SET_ATTEMPTED_PASSWORD": {
            state = {
                ...state,
                attemptedValues: {
                    ...state.attemptedValues,
                    password: action.payload
                }
            };
            break;
        }
        case "USER_AUTH_SET_LOGIN_STATUS": {
            state = {
                ...state,
                loggedIn: action.payload,
            };
            break;
        }
    }
    return state;
}