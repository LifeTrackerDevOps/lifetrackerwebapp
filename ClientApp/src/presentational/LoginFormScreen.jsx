import React from "react";

export default class LoginFormScreen extends React.Component {
    render(){
        return (
            <div>
                <h2>Login</h2>
                <form onSubmit={this.props.onSubmitFunction}>
                    <input type="email" onChange={this.props.onChangeUsernameFunction} placeholder="Email" required/>
                    <input type="password" onChange={this.props.onChangePasswordFunction} placeholder="Password" required/>
                    <input type="submit" value="Enter"/>
                </form>
            </div>
        );
    }
}