import React from "react";

export default class RegisterFormScreen extends React.Component {
    render(){
        return (
            <form onSubmit={this.props.onSubmitFunction}>
                <input type="email" value={this.props.emailValue} onChange={this.props.onChangeEmailFunction} placeholder="Email" required/>
                <input type="password" value={this.props.passwordValue} onChange={this.props.onChangePasswordFunction} placeholder="Password" required/>
                <input type="password" value={this.props.confirmPasswordValue} onChange={this.props.onChangeConfirmPasswordFunction} placeholder="Confirm Password" required/>
                <input type="submit" value="Enter" disabled={!this.props.canSubmit}/>
            </form>
        );
    }
}