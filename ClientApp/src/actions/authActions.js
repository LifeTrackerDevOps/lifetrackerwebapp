import axios from "axios";
import Cookies from "js-cookie";

export function attemptLogin(email, password) {
    return function(dispatch) {
        let formData = new FormData();
        formData.append("email", email);
        formData.append("password", password);

        axios({
            method: "post",
            url: "/api/login",
            headers: {
                "X-CSRFToken": Cookies.get("csrftoken")
            },
            data: formData
          }).then((response) => {
                dispatch({type: "USER_AUTH_FULFILLED"});
          }).catch((error) => {
                dispatch({type: "USER_AUTH_REJECTED"});
          });
    }
}
export function attemptLogout() {
    return function(dispatch) {
        axios({
            method: "post",
            url: "/api/logout",
            headers: {
                "X-CSRFToken": Cookies.get("csrftoken")
            }
          }).then((response) => {
                dispatch({
                    type: "USER_AUTH_SET_LOGIN_STATUS",
                    payload: false
                });
          }).catch((error) => {
          });
    }
}
export function setLoginStatus(status) {
    return {
        type: "USER_AUTH_SET_LOGIN_STATUS",
        payload: status
    }
}
export function setEmailField(event){
    return {
        type: "USER_AUTH_SET_ATTEMPTED_EMAIL",
        payload: event.target.value
    }
}
export function setPasswordField(event){
    return {
        type: "USER_AUTH_SET_ATTEMPTED_PASSWORD",
        payload: event.target.value
    }
}