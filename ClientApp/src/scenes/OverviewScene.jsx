import React from "react";

import { Link } from "react-router-dom";

import { connect } from "react-redux";

@connect((store) => {
    return {
        auth: store.auth
    }
})
export default class OverviewScene extends React.Component {
    render() {
        return (
            <div>
                <h1>Overview!</h1>
            </div>
        );
    }
}