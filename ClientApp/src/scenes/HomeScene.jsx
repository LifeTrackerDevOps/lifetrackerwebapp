import React from "react";

import LoginForm from "../containers/LoginForm";

import { Link, BrowserRouter } from "react-router-dom";

import { connect } from "react-redux";

import { attemptLogout } from "../actions/authActions";

@connect((store) => {
    return {
        auth: store.auth
    }
})
export default class HomeScene extends React.Component {
    constructor(props) {
        super(props);
        this.handleLogout = this.handleLogout.bind(this);
    }
    handleLogout(){
        this.props.dispatch(attemptLogout())
    }
    render() {
        if(!this.props.auth.loggedIn && !this.props.auth.justRejected) {
            return (
                <div>
                    <h1>Welcome to LifeTracker</h1>
                    <LoginForm/>
                    <Link to="/overview">o</Link>
                    <Link to="/register">Register</Link>
                </div>
            );
        }
        if(this.props.auth.justRejected) {
            return (
                <div>
                    <h1>Welcome to LifeTracker</h1>
                    <LoginForm/>
                    <h2>There was an error logging in</h2>
                    <Link to="/register">Register</Link>
                </div>
            );
        }
        return (
            <div>
                <h1>Logged in!</h1>
                <Link to="/overview">Overview</Link>
                <button onClick={this.handleLogout}>Logout!</button>
            </div>
        );
    }
}