const webpack = require("webpack");

const config = {
    entry: __dirname + '/src/index.jsx',
    output: {
        path: __dirname + '/dist',
        filename: 'bundle.js',
    },
    resolve: {
        extensions: ['.js', '.jsx']
    },
    module: {
        rules: [
            {
                test: /\.jsx?/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                      plugins: ['transform-object-rest-spread', 'transform-decorators-legacy']
                    }
                  }
            }
        ]
    }
};
module.exports = config;