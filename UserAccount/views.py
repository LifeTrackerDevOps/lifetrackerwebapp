from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseForbidden
from django.views.decorators.http import require_GET, require_POST

from django.contrib.auth import authenticate, logout as LifeTrackerLogout, login as LifeTrackerLogin
from django.contrib.auth.decorators import login_required

from .models import LifeTrackerUser

@require_POST
def login(request):
    if request.user.is_authenticated:
        return HttpResponseForbidden()
    attempted_email = request.POST.get("email", None)
    attempted_password = request.POST.get("password", None)
    if attempted_email is not None and attempted_password is not None:
        user = authenticate(username = attempted_email, password = attempted_password)
        if user is not None:
            request.session.set_expiry(86400)
            LifeTrackerLogin(request, user)
            response = HttpResponse()
            return response
        else:
            return HttpResponseBadRequest()
    else:
        return HttpResponseBadRequest()
@require_POST
def register(request):
    if request.user.is_authenticated:
        return HttpResponseForbidden()
    email = request.POST.get("email", None)
    password = request.POST.get("password", None)
    if email is not None and password is not None:
        user = LifeTrackerUser.objects.create_user(email=email, password=password)
        user.save()
        return HttpResponse()
    else:
        return HttpResponseBadRequest()
@login_required
@require_POST
def logout(request):
    LifeTrackerLogout(request)
    return HttpResponse()