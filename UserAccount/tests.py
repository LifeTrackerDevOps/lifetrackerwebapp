from django.test import TestCase
from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseForbidden
from django.urls import reverse
from django.core.exceptions import ObjectDoesNotExist, DisallowedHost

from .views import login
from .models import LifeTrackerUser as User
class UserAccountGeneralLoggedInTestCase(TestCase):
    def setUp(self):
        self.client.force_login(User.objects.get_or_create(email="test@test.com")[0])
    def test_view_login_already_authorized_returns_forbidden(self):
        response = self.client.post(reverse("login"))
        self.assertIsInstance(response, HttpResponseForbidden)
    def test_view_register_alread_authorized_returns_forbidden(self):
        response = self.client.post(reverse("register"))
        self.assertIsInstance(response, HttpResponseForbidden)
    def test_view_logout_authorized_returns_ok(self):
        response = self.client.post(reverse("logout"))
        self.assertIsInstance(response, HttpResponse)
class UserAccountLoginTestCase(TestCase):
    def setUp(self):
        User.objects.create_user("test@test.com", "1234")
    def test_view_login_valid_credentials_returns_ok_and_a_session_cookie(self):
        data = {"email": "test@test.com", "password": "1234" }
        response = self.client.post(path=reverse("login"), data=data)
        self.assertIsInstance(response, HttpResponse)
        self.assertIsNotNone(response.client.cookies["sessionid"])
    def test_view_login_invalid_credentials_returns_bad_request_and_no_session_cookie(self):
        data = {"email": "test@test.com", "password": "12345" }
        response = self.client.post(path=reverse("login"), data=data)
        self.assertIsInstance(response, HttpResponseBadRequest)
        with self.assertRaises(KeyError):
            response.client.cookies["sessionid"]
    def test_view_login_no_credentials_returns_bad_request_and_no_session_cookie(self):
        response = self.client.post(path=reverse("login"))
        self.assertIsInstance(response, HttpResponseBadRequest)
        with self.assertRaises(KeyError):
            response.client.cookies["sessionid"]
class UserAccountRegisterTestCase(TestCase):
    def test_view_register_valid_input_returns_ok_and_user_created(self):
        data = {"email": "test@test.com", "password": "1234" }
        response = self.client.post(path=reverse("register"), data=data)
        testUser = User.objects.get(email="test@test.com")
        self.assertIsInstance(response, HttpResponse)
        self.assertIsNotNone(testUser)
    def test_view_register_no_input_returns_bad_request_and_no_user_created(self):
        response = self.client.post(path=reverse("register"))
        self.assertIsInstance(response, HttpResponse)
        with self.assertRaises(ObjectDoesNotExist):
            User.objects.get(email="test@test.com")