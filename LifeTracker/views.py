from django.http import HttpResponse
from django.views.decorators.http import require_GET, require_POST
from django.template import loader

import datetime

@require_GET
def index(request):
    template = loader.get_template('index.html')
    context = {
        'title': 'Ben'
    }
    response = HttpResponse(template.render(context, request))
    if request.user.is_authenticated and request.COOKIES.get("ln_flag", None) is None:
        expires = datetime.datetime.strftime(datetime.datetime.utcnow() + datetime.timedelta(seconds=86400), "%a, %d-%b-%Y %H:%M:%S GMT")
        response.set_cookie("ln_flag", "aa", expires=expires)
    elif request.user.is_authenticated is False and request.COOKIES.get("ln_flag", None) is not None:
        response.delete_cookie("ln_flag")
    return response
@require_POST
def login(request):
    return HttpResponse("Test")